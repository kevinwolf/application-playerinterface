'use strict';

var path         = require('path')
  , webpack      = require('webpack')
  , node_modules = path.join(__dirname, '../node_modules')
  , src          = path.join(__dirname, '../app')
  , build        = path.join(__dirname, '../build')
  , config;

config = {
  target    : 'web',
  cache     : true,
  entry     : {
    app    : [ 'webpack-dev-server/client?http://0.0.0.0:8080', 'webpack/hot/only-dev-server', './app/app.js' ],
    vendor : [ 'react', 'react-router', 'react-tap-event-plugin', 'alt', 'axios', 'classnames' ]
  },
  resolve   : {
    modulesDirectories : [ 'node_modules', 'app' ]
  },
  output    : {
    path     : path.join(__dirname, '../build'),
    filename : 'bundle.js'
  },
  module    : {
    loaders : [
      { test : /\.js$/, loader : 'react-hot!babel', exclude : /node_modules/ },
      { test : /\.scss$/, loader : 'style!css!sass' },
      { test : /\.(png|jpg)$/, loader : 'url?limit=25000' }
    ]
  },
  plugins   : [
    new webpack.optimize.CommonsChunkPlugin('vendor', 'vendor.bundle.js'),
    new webpack.DefinePlugin({
      API_URL : JSON.stringify(process.env.NODE_ENV === 'production' ? 'http://190.241.12.163:1102' : 'http://190.241.12.163:11020'),
    }),
  ]
};

module.exports = config;
