#PlayerInterface Application

##Developing
1. Run `npm install`.
2. Create a new feature branch of the feature you will be working on related to the Jira ticket. (For example `feature/AL-101-create-a-new-page`).
3. Push the feature branch to Bitbucket and **create a pull request**. Add other developers as a reviewers and **wait them to approve the changes**.
4. Once the developers approved the changes, merge the feature branch with `develop`.

##Deploying
Deploys happen automatically when pushing to `develop` or `master`.

###Staging URL
http://190.241.12.163:12020/

###Production URL
http://190.241.12.163:1202/
