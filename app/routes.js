// Third parties.
import React from 'react';
import { Route } from 'react-router';
// import { Route, DefaultRoute } from 'react-router';

// Layouts.
import Root from 'components/layouts/Root';
import App from 'components/layouts/App';

// Pages.
import Login from 'components/pages/Login';

// Routes definition.
const Routes = (
  <Route handler={Root}>
    <Route name="app" path="/" handler={App} />
    <Route name="login" handler={Login} />
  </Route>
);

export default Routes;
