import React from 'react';
// import { RouteHandler } from 'react-router';
import AuthStore from 'stores/AuthStore';
import AuthActions from 'actions/AuthActions';
require('./style.scss');

class App extends React.Component {

  static contextTypes = {
    router : React.PropTypes.func,
  }

  render () {
    return (
      <div>APP</div>
    );
  }

  // If the token doesn't exist, then send the player back
  // to the login screen.
  static willTransitionTo (transition) {
    if (!AuthStore.state.token) {
      transition.redirect('login');
    }
  }

  // Send to log in screen.
  _logOut = () => {
    AuthActions.logout();
  }

}

export default App;
