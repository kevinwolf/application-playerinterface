import React, { Component } from 'react';
import classNames from 'classnames';
require('./style.scss');

export default class Column extends Component {

  static propTypes = {
    'children'   : React.PropTypes.node.isRequired,
    'text-right' : React.PropTypes.bool,
  }

  constructor (props) {
    super(props);
  }

  render () {
    const classes = classNames({
      'Grid__Column'             : true,
      'Grid__Column--text-right' : this.props['text-right'],
    });

    return <div className={classes}>{this.props.children}</div>;
  }

}
