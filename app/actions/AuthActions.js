import { createActions } from 'alt/utils/decorators';
import flux from 'flux';
import RouterService from 'services/RouterService';

@createActions(flux)
class AuthActions {

  constructor () {
    this.generateActions('authenticate');
    this.generateActions('setToken');
  }

  loginSuccessful (response) {
    RouterService.get().transitionTo('app');
    AuthActions.setToken(response.data.token);
  }

  logout () {
    RouterService.get().transitionTo('login');
    this.dispatch();
  }

}

export default AuthActions;
